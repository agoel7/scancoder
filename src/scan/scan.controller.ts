import { Controller, Get, Post } from "@nestjs/common";
import { ScanService } from "./scan.service";

@Controller()
export class ScanController{
    constructor(private scanService: ScanService){}

    @Post('scanRepository')
    scanCode(){
        this.scanService.scanCode()
    }

    @Get('scanResult')
    scanResult(){
        this.scanService.scanResult()
    }

}